#!/usr/bin/env php
<?php

// Check if the "Game Name" argument is provided
if ($argc < 2) {
    echo "Usage: php script.php <Game Name> <Different Spelling of Game Name>\n";
    exit(1);
}

// Extract the "Game Name" argument
$gameName = $argv[1];

// Connect to the SQLite database
$db_path = 'RHDN.sqlite.db'; // Path to the SQLite database file
$pdo = new PDO("sqlite:$db_path"); // Create a PDO instance to connect to the database
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set error mode to exceptions for better error handling

// Execute the SQL query for specific console names and the provided game name
$sql_query = "
    SELECT hack_id, orig_game, romhack_title, console_name, category, base_rom, img_title, img_snap, patches
    FROM hacks
    WHERE patches IS NOT NULL AND patches != ''
        AND console_name IN ('NES', 'SNES', 'GB', 'GBA', 'N64', 'GG', 'GEN', 'VB', 'SGFX', 'JAG', 'LYNX', '32X')
        AND (orig_game LIKE :gameName ESCAPE '\\') COLLATE NOCASE
    ORDER BY console_name, category, orig_game, romhack_title
"; // SQL query to retrieve relevant data from the database

$stmt = $pdo->prepare($sql_query); // Prepare the SQL query with a placeholder for the game name
$stmt->bindValue(':gameName', "%$gameName%", PDO::PARAM_STR); // Bind the game name parameter
$stmt->execute(); // Execute the SQL query
$data = $stmt->fetchAll(PDO::FETCH_ASSOC); // Fetch all results into an associative array

// Define the console mapping
$consoleMapping = array(
    '32X' => 'Sega - 32X',
    '3DO' => 'The 3DO Company - 3DO',
    'DC' => 'Sega - Dreamcast',
    'GB' => 'Nintendo - Game Boy',
    'GBA' => 'Nintendo - Game Boy Advance',
    'GC' => 'Nintendo - GameCube',
    'GEN' => 'Sega - Mega Drive - Genesis',
    'GG' => 'Sega - Game Gear',
    'JAG' => 'Atari - Jaguar',
    'LYNX' => 'Atari - Lynx',
    'N64' => 'Nintendo - Nintendo 64',
    'NDS' => 'Nintendo - Nintendo DS',
    'NES' => 'Nintendo - Nintendo Entertainment System',
    'NGCD' => 'SNK - Neo Geo CD',
    'NGPC' => 'SNK - Neo Geo Pocket Color',
    'PMINI' => 'Nintendo - Pokemon Mini',
    'PS2' => 'Sony - PlayStation 2',
    'PS3' => 'Sony - PlayStation 3',
    'PSP' => 'Sony - PlayStation Portable',
    'PSX' => 'Sony - PlayStation',
    'SAT' => 'Sega - Saturn',
    'SG1K' => 'Sega - SG-1000',
    'SGFX' => 'NEC - PC Engine SuperGrafx',
    'SMS' => 'Sega - Master System - Mark III',
    'SNES' => 'Nintendo - Super Nintendo Entertainment System',
    'SegaCD' => 'Sega - Mega-CD - Sega CD',
    'VB' => 'Nintendo - Virtual Boy',
);

// Define the base path for various operations
$base_path = "/home/stoned/Games/Romhacks/romhacking.net/TRANSLATIONS/"; // Base path for ROM hacks
$playlist_path = "/home/stoned/Games/retroarch/playlists/"; // Path for RetroArch playlists

// Define core paths and names based on console names
$corePaths = array(
    '32X' => '~/Games/retroarch/cores/picodrive_libretro.so',
    '3DO' => '~/Games/retroarch/cores/4do_libretro.so',
    'DC' => '~/Games/retroarch/cores/flycast_libretro.so',
    'GB' => '~/Games/retroarch/cores/gambatte_libretro.so',
    'GBA' => '~/Games/retroarch/cores/mgba_libretro.so',
    'GC' => '~/Games/retroarch/cores/dolphin_libretro.so',
    'GEN' => '~/Games/retroarch/cores/genesis_plus_gx_libretro.so',
    'GG' => '~/Games/retroarch/cores/gearboy_libretro.so',
    'JAG' => '~/Games/retroarch/cores/virtualjaguar_libretro.so',
    'LYNX' => '~/Games/retroarch/cores/mednafen_lynx_libretro.so',
    'N64' => '~/Games/retroarch/cores/mupen64plus_next_libretro.so',
    'NDS' => '~/Games/retroarch/cores/melonds_libretro.so',
    'NES' => '~/Games/retroarch/cores/fceumm_libretro.so',
    'NGCD' => '~/Games/retroarch/cores/neocd_libretro.so',
    'NGPC' => '~/Games/retroarch/cores/mednafen_ngp_libretro.so',
    'PMINI' => '~/Games/retroarch/cores/tigb_libretro.so',
    'PS2' => '~/Games/retroarch/cores/pcsx2_libretro.so',
    'PS3' => '~/Games/retroarch/cores/rpcs3_libretro.so',
    'PSP' => '~/Games/retroarch/cores/ppsspp_libretro.so',
    'PSX' => '~/Games/retroarch/cores/pcsx_rearmed_libretro.so',
    'SAT' => '~/Games/retroarch/cores/yabause_libretro.so',
    'SG1K' => '~/Games/retroarch/cores/genesis_plus_gx_libretro.so',
    'SGFX' => '~/Games/retroarch/cores/mednafen_supergrafx_libretro.so',
    'SMS' => '~/Games/retroarch/cores/smsplus_libretro.so',
    'SNES' => '~/Games/retroarch/cores/snes9x_libretro.so',
    'SegaCD' => '~/Games/retroarch/cores/genesis_plus_gx_libretro.so',
    'VB' => '~/Games/retroarch/cores/mednafen_vb_libretro.so',
);

$coreNames = array(
    '32X' => 'Sega - MS/GG/MD/CD/32X (PicoDrive)',
    '3DO' => 'The 3DO Company - 3DO',
    'DC' => 'Sega - Dreamcast',
    'GB' => 'Nintendo - Game Boy / Color (Gambatte)',
    'GBA' => 'Nintendo - Game Boy Advance (mGBA)',
    'GC' => 'Nintendo - GameCube',
    'GEN' => 'Sega - MS/GG/MD/CD (Genesis Plus GX)',
    'GG' => 'Sega - Game Gear (Gearboy)',
    'JAG' => 'Atari - Jaguar (VirtualJaguar)',
    'LYNX' => 'Atari - Lynx (Mednafen Lynx)',
    'N64' => 'Nintendo - Nintendo 64 (Mupen64 Plus Next)',
    'NDS' => 'Nintendo - Nintendo DS (melonDS)',
    'NES' => 'Nintendo - NES / Famicom (FCEUmm)',
    'NGCD' => 'SNK - Neo Geo CD (NeoCD)',
    'NGPC' => 'SNK - Neo Geo Pocket Color (Mednafen NGP)',
    'PMINI' => 'Nintendo - Pokemon Mini (TGB Dual)',
    'PS2' => 'Sony - PlayStation 2 (PCSX2)',
    'PS3' => 'Sony - PlayStation 3 (RPCS3)',
    'PSP' => 'Sony - PlayStation Portable (PPSSPP)',
    'PSX' => 'Sony - PlayStation (PCSX ReARMed)',
    'SAT' => 'Sega - Saturn (Yabause)',
    'SG1K' => 'Sega - SG-1000 (Genesis Plus GX)',
    'SGFX' => 'NEC - PC Engine SuperGrafx (Mednafen SuperGrafx)',
    'SMS' => 'Sega - Master System - Mark III (SMSPlus)',
    'SNES' => 'Nintendo - SNES / SFC (Snes9x)',
    'SegaCD' => 'Sega - Mega-CD - Sega CD (Genesis Plus GX)',
    'VB' => 'Nintendo - Virtual Boy (Mednafen VB)',
);

// Iterate through data and generate playlists
foreach ($data as $entry) {
    $console_name = $entry['console_name'];
    $console_full_name = $consoleMapping[$console_name];
    $category = $entry['category'];
    $playlist_name = "Romhacks RHDN - $gameName.lpl"; // Use the provided game name in the playlist name

    $playlist_full_path = $playlist_path . $playlist_name;

    $snap_path = "/home/stoned/Games/retroarch/thumbnails/Romhacks RHDN - $gameName/Named_Snaps/";
    $title_path = "/home/stoned/Games/retroarch/thumbnails/Romhacks RHDN - $gameName/Named_Titles/";

    $playlist_json = [
        "version" => "1.5",
        "default_core_path" => $corePaths[$console_name],
        "default_core_name" => $coreNames[$console_name],
        "label_display_mode" => 0,
        "right_thumbnail_mode" => 3,
        "left_thumbnail_mode" => 2,
        "thumbnail_match_mode" => 0,
        "sort_mode" => 0,
        "scan_content_dir" => $base_path . $console_name,
        "scan_file_exts" => "7z zip rar",
        "scan_dat_file_path" => "",
        "scan_search_recursively" => true,
        "scan_search_archives" => false,
        "scan_filter_dat_content" => false,
        "scan_overwrite_playlist" => false,
        "items" => []
    ];

    $existing_content = file_exists($playlist_full_path) ? file_get_contents($playlist_full_path) : null;

    $existing_json = $existing_content ? json_decode($existing_content, true) : ["items" => []];

    foreach (json_decode($entry['patches']) as $patch) {
        $patch_path = trim($patch);
        $label = "{$entry['console_name']} - {$entry['hack_id']} -  {$entry['orig_game']} - {$entry['romhack_title']} -" . basename($patch);

        $item = [
            "path" => $patch_path,
            "label" => $label,
            "core_path" => $corePaths[$console_name],
            "core_name" => $coreNames[$console_name],
            "crc32" => "00000000|crc",
            "db_name" => $playlist_name
        ];

        $existing_json["items"][] = $item;
    }

    $playlist_json = array_merge($playlist_json, $existing_json);

    file_put_contents($playlist_full_path, json_encode($playlist_json, JSON_PRETTY_PRINT));

    $img_snap_src = $base_path . 'TRANSLATIONS_IMAGES/snaps/' . $entry['img_snap'];
    $img_title_src = $base_path . 'TRANSLATIONS_IMAGES/titles/' . $entry['img_title'];

    if (file_exists($img_snap_src)) {
        $snap_dest_dir = $snap_path;
        mkdir($snap_dest_dir, 0777, true);
        $snap_dest_file = $snap_dest_dir . $label . '.' . pathinfo($entry['img_snap'], PATHINFO_EXTENSION);
        copy($img_snap_src, $snap_dest_file);
    }

    if (file_exists($img_title_src)) {
        $title_dest_dir = $title_path;
        mkdir($title_dest_dir, 0777, true);
        $title_dest_file = $title_dest_dir . $label . '.' . pathinfo($entry['img_title'], PATHINFO_EXTENSION);
        copy($img_title_src, $title_dest_file);
    }
}

$pdo = null; // Close the database connection

echo "Playlists and images copied successfully.\n";
?>
