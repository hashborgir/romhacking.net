<?php

// Specify the directory to search recursively
$directory = '/home/stoned/Games/Roms/SNES/USA/';

// Specify the SQLite database file
$databaseFile = 'sqlite.SNES.db';

// Initialize SQLite database connection
try {
    $pdo = new PDO('sqlite:' . $databaseFile);
} catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}

// Create the top200 table if it doesn't exist
$createTableQuery = "CREATE TABLE IF NOT EXISTS top200 (id INTEGER PRIMARY KEY AUTOINCREMENT, filename TEXT)";
$pdo->exec($createTableQuery);

// Prepare the SQLite insertion query
$insertQuery = "INSERT INTO top200 (filename) VALUES (:filename)";
$statement = $pdo->prepare($insertQuery);

// Function to recursively find .zip files and insert into the database
function processDirectory($directory, $statement) {
    $files = glob($directory . '*', GLOB_MARK | GLOB_NOSORT);

    foreach ($files as $file) {
        if (is_dir($file)) {
            processDirectory($file, $statement);
        } elseif (pathinfo($file, PATHINFO_EXTENSION) === 'zip') {
            $statement->bindParam(':filename', $file, PDO::PARAM_STR);
            $statement->execute();
        }
    }
}

// Call the function to process the directory
processDirectory($directory, $statement);

// Close the database connection
$pdo = null;

echo "Script executed successfully.\n";

?>
