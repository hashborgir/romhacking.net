#!/usr/bin/env python

import sqlite3
from urllib.parse import urlparse

# Connect to the SQLite database
conn = sqlite3.connect('RHDN.translations.sqlite.db')
cursor = conn.cursor()

# Fetch all rows from the 'hacks' table
cursor.execute("SELECT rowid, url_title, url_snap FROM hacks")
rows = cursor.fetchall()

# Update each row
for row in rows:
    # Extract filename from url_title
    img_title = row[1].split('/')[-1]

    # Extract filename from url_snap (considering it's a comma-separated list)
    img_snap = row[2].split(',')[0].split('/')[-1]

    # Log the processing for each row
    print(f"Processing row {row[0]}:")
    print(f"  Original url_title: {row[1]}")
    print(f"  Extracted img_title: {img_title}")
    print(f"  Original url_snap: {row[2]}")
    print(f"  Extracted img_snap: {img_snap}")

    # Update the row with the new filenames
    cursor.execute("UPDATE hacks SET img_title=?, img_snap=? WHERE rowid=?", (img_title, img_snap, row[0]))

# Commit the changes and close the connection
conn.commit()
conn.close()
