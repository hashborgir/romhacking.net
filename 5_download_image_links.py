#!/usr/bin/python

import os
import sqlite3
import requests
from urllib.parse import urlparse
from concurrent.futures import ThreadPoolExecutor

# Function to download and save an image
def download_image(url, destination):
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        with open(destination, 'wb') as img_file:
            for chunk in response.iter_content(chunk_size=128):
                img_file.write(chunk)

# Function to process a row
def process_row(row):
    hack_id, img_title_src, img_snap_src = row

    # Download and save images
    with ThreadPoolExecutor(max_workers=64) as executor:
        futures = []

        for img_src in img_title_src.split(','):
            img_filename = os.path.basename(urlparse(img_src).path)
            img_path = os.path.join('IMAGES', 'titles', f'{hack_id}_{img_filename}')
            futures.append(executor.submit(download_image, img_src, img_path))

        for img_src in img_snap_src.split(','):
            img_filename = os.path.basename(urlparse(img_src).path)
            img_path = os.path.join('IMAGES', 'snaps', f'{hack_id}_{img_filename}')
            futures.append(executor.submit(download_image, img_src, img_path))

        # Wait for all threads to complete
        for future in futures:
            future.result()

    print(f"Downloaded images for hack_id {hack_id}")

# Read hack_ids and image sources from the hacks table
with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
    cursor = conn.cursor()
    cursor.execute('SELECT hack_id, url_title, url_snap FROM hacks')
    rows = cursor.fetchall()

# Process each row
for row in rows:
    process_row(row)

print("Done downloading images.")
