#!/usr/bin/python

import sqlite3
import requests
from http.cookiejar import MozillaCookieJar
from concurrent.futures import ThreadPoolExecutor

# Load cookies from the file
cookie_jar = MozillaCookieJar('cookies-romhacking-net.txt')
cookie_jar.load()

# Function to get suggested download name for a hack_id
def get_download_name(hack_id):
    url = f'https://www.romhacking.net/download/translations/{hack_id}/'
    headers = {'Referer': url}
    response = requests.head(url, cookies=cookie_jar, headers=headers)

    # Check if the request was successful
    if response.status_code == 200:
        # Extract suggested download name from the Content-Disposition header
        content_disposition = response.headers.get('Content-Disposition')
        if content_disposition:
            download_name = content_disposition.split('filename=')[1].strip('"')
            return download_name
        else:
            print(f"Content-Disposition header not found for hack_id {hack_id}")
            return None
    else:
        print(f"Failed to fetch headers for hack_id {hack_id}. Status code: {response.status_code}")
        return None

# Function to process a batch of hack_ids
def process_hack_ids(batch_hack_ids):
    # Connect to the SQLite database
    conn = sqlite3.connect('RHDN.translations.sqlite.db')
    cursor = conn.cursor()

    # Process each hack_id in the batch
    for hack_id in batch_hack_ids:
        hack_id = hack_id[0]  # Extract the numerical hack_id from the tuple
        download_name = get_download_name(hack_id)

        if download_name is not None:
            # Update the romhack_file column in the database
            cursor.execute('UPDATE hacks SET romhack_file = ? WHERE hack_id = ?', (download_name, hack_id))
            conn.commit()
            print(f"Updated romhack_file for hack_id {hack_id} - {download_name}")

    # Close the database connection
    conn.close()

# Connect to the SQLite database
conn = sqlite3.connect('RHDN.translations.sqlite.db')
cursor = conn.cursor()

# Select hack_ids from the hacks table
cursor.execute('SELECT hack_id FROM hacks')
hack_ids = cursor.fetchall()

# Close the database connection
conn.close()

# Process hack_ids using ThreadPoolExecutor with 16 threads
batch_size = 16
with ThreadPoolExecutor(max_workers=16) as executor:
    for i in range(0, len(hack_ids), batch_size):
        batch_hack_ids = hack_ids[i:i + batch_size]
        executor.submit(process_hack_ids, batch_hack_ids)
