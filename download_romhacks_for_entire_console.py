#!/usr/bin/python
import requests
from bs4 import BeautifulSoup
import sys

def scrape_table(url, console_name):
    # Send a GET request to the URL
    response = requests.get(url)
    if response.status_code != 200:
        print(f"Failed to fetch the page. Status Code: {response.status_code}")
        return None

    # Parse the HTML content of the page using BeautifulSoup
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find the table in the HTML
    table = soup.find('table')

    if not table:
        print("No table found on the page.")
        return None

    # Create a hashmap to store the data
    data_hashmap = {}

    # Iterate through rows in the table, starting from the second row (skipping the header)
    for row in table.find_all('tr')[1:]:
        # Extract data from each column in the row
        columns = row.find_all(['td', 'th'])
        if columns:
            # Extract values from each column
            url_href = columns[0].find('a').get('href', '')
            title = columns[0].find('a').text.strip()
            released_by = columns[1].find('a').text.strip()
            genre = columns[2].text.strip()
            platform = columns[3].text.strip()
            status = columns[4].text.strip()
            ver = columns[5].text.strip()
            date = columns[6].text.strip()
            lang = columns[7].text.strip()

            # Save data to the hashmap
            data_hashmap[url_href] = {
                'url': url_href,

                'original_game': title,
                'title': title,
                'released_by': released_by,

                'genre': genre,
                'platform': platform,
                'mods': '',
                'ver': ver,

                'category': status,
                'date': date,
            }

    # Save the hashmap to a text file named after the console_name
    file_name = f"{console_name}.txt"
    with open(file_name, 'a+') as file:
        for url, game_data in data_hashmap.items():
            file.write(f"{url}|{game_data['title']}|{game_data['original_game']}|{game_data['released_by']}|{game_data['genre']}|{game_data['platform']}|{game_data['mods']}|{game_data['ver']}|{game_data['category']}|{game_data['date']}\n")

    print(f"Data has been saved to {file_name}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py <LIST PAGE URL> <console_name>")
    else:
        url = sys.argv[1]
        console_name = sys.argv[2]
        scrape_table(url, console_name)
