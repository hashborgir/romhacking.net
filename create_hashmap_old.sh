#!/bin/bash

# Define the ROMs directory
roms_dir=$HOME/Games/Roms

# Input file
romslist_file=$roms_dir/romslist.txt

# Output file
hashmap_file=$roms_dir/hashmap.txt
hashmap_md5_file=$roms_dir/hashmap.md5.txt
hashmap_sha1_file=$roms_dir/hashmap.sha1.txt

# Create /tmp/Roms directory
tmp_dir=/dmt/tmp
mkdir -p $tmp_dir

# Ensure the output file is empty
> $hashmap_file

# Loop through each rom file in romslist.txt
while IFS= read -r rom_file
do
    # Clear out the contents of /tmp/Roms
    rm -rf "$tmp_dir"/*

    # Extract file extension
    extension="${rom_file##*.}"

    # Unpack the rom into /tmp/Roms based on file extension
    case "$extension" in
        7z)
            7z x "$rom_file" -o"$tmp_dir"
            ;;
        zip)
            unzip "$rom_file" -d "$tmp_dir"
            ;;
        *)
            echo "Unsupported file format: $extension. Skipping."
            continue
            ;;
    esac

    # Loop through each file in /tmp/Roms
    for tmp_file in "$tmp_dir"/*
    do
        # Calculate sha1sum
        crc32sum=$(crc32 "$tmp_file" | awk '{print $1}')
        md5sum=$(md5sum "$tmp_file" | awk '{print $1}')
        sha1sum=$(sha1sum "$tmp_file" | awk '{print $1}')

        # Append to hashmaps
        echo "$crc32sum:$rom_file" >> $hashmap_file
        echo "$md5sum:$rom_file" >> $hashmap_md5_file
        echo "$sha1sum:$rom_file" >> $hashmap_sha1_file

    done
done < $romslist_file

echo "Task 2 completed. hashmap.txt has been updated with sha1sum and corresponding rom file locations."
