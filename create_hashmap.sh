#!/bin/bash

# Define the ROMs directory
roms_dir=$HOME/Games/Roms

# Input file
romslist_file=$roms_dir/romslist.txt

# Output file
hashmap_file=$roms_dir/hashmap.txt
hashmap_md5_file=$roms_dir/hashmap.md5.txt
hashmap_sha1_file=$roms_dir/hashmap.sha1.txt

# Create /tmp/Roms directory
tmp_dir=/tmp/Roms
mkdir -p "$tmp_dir"

# Ensure the output file is empty
> "$hashmap_file"
> "$hashmap_md5_file"
> "$hashmap_sha1_file"

# Function to calculate hashes and append to hashmaps
calculate_hashes() {
    local tmp_file="$1"
    local tmp_file_noheader="$2"
    local rom_file="$3"

    # Calculate original file hashes
    crc32sum=$(crc32 "$tmp_file" | awk '{print $1}')
    md5sum=$(md5sum "$tmp_file" | awk '{print $1}')
    sha1sum=$(sha1sum "$tmp_file" | awk '{print $1}')

    # Append to original file hashmaps
    echo "$crc32sum:$rom_file" >> "$hashmap_file"
    echo "$md5sum:$rom_file" >> "$hashmap_md5_file"
    echo "$sha1sum:$rom_file" >> "$hashmap_sha1_file"

    # Calculate noheader file hashes
    crc32sum_noheader=$(crc32 "$tmp_file_noheader" | awk '{print $1}')
    md5sum_noheader=$(md5sum "$tmp_file_noheader" | awk '{print $1}')
    sha1sum_noheader=$(sha1sum "$tmp_file_noheader" | awk '{print $1}')

    # Append to noheader file hashmaps
    echo "$crc32sum_noheader:$rom_file" >> "$hashmap_file"
    echo "$md5sum_noheader:$rom_file" >> "$hashmap_md5_file"
    echo "$sha1sum_noheader:$rom_file" >> "$hashmap_sha1_file"
}


# Loop through each rom file in romslist.txt
while IFS= read -r rom_file
do
    # Clear out the contents of /tmp/Roms
    rm -rf "$tmp_dir"/*

    # Extract file extension
    rom_file_extension="${rom_file##*.}"

    # Unpack the rom into /tmp/Roms based on file extension
    case "$rom_file_extension" in
        7z)
            7z x "$rom_file" -o"$tmp_dir"
            ;;
        zip)
            unzip "$rom_file" -d "$tmp_dir"
            ;;
        rar)
            unrar x "$rom_file" "$tmp_dir"
            ;;
        *)
            echo "Unsupported file format: $rom_file_extension. Skipping."
            continue
            ;;
    esac

    # Loop through each file in /tmp/Roms
    for tmp_file in "$tmp_dir"/*
    do
        extension="${tmp_file##*.}"
        tmp_file_noheader="$tmp_file.noheader"

        echo $tmp_file
        echo $tmp_file_noheader

        # Calculate hashes and append to hashmaps based on file extension
        case "$extension" in
            nes)
                tail -c +17 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"
                ;;
            smc|sfc|sns)
                tail -c +513 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"
                ;;
            gen)
                tail -c +257 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"
                ;;
            n64|z64|v64)
                tail -c +65 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"
                ;;
            gb)
                tail -c +45 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"

                tail -c +49 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"
                ;;
            gbc)
                tail -c +49 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"
                ;;
            gba)
                tail -c +193 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"
                ;;
            gg)
                tail -c +17 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"

                tail -c +33 "$tmp_file" > "$tmp_file_noheader"
                calculate_hashes "$tmp_file" "$tmp_file_noheader" "$rom_file"
                ;;
        esac
    done
done < "$romslist_file"

echo "Task 2 completed. Hashmaps have been updated with sha1sum and corresponding rom file locations."
