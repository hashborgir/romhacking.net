<?php

// Given SNES games array
$snesGames = [
    200 => "Final Fight 3",
    199 => "Pocky & Rocky 2",
    198 => "Super Empire Strikes Back",
    197 => "Ken Griffey Jr. Presents Major League Baseball",
    196 => "Joe and Mac",
    195 => "NHL 94",
    194 => "Rock N’ Roll Racing",
    193 => "Mario Paint",
    192 => "Super Bomberman",
    191 => "The Lost Vikings",
    190 => "Earthworm Jim 2",
    189 => "Shadowrun",
    188 => "Pilotwings",
    187 => "E.V.O.: Search for Eden",
    186 => "Lufia 2: Rise of the Sinistrals",
    185 => "NHLPA ’93",
    184 => "Secret of Evermore",
    183 => "Final Fight 2",
    182 => "Final Fantasy: Mystic Quest",
    181 => "Super Star Wars",
    180 => "Saturday Night Slam Masters",
    179 => "Final Fantasy II (IV in Japan)",
    178 => "NCAA Football",
    177 => "BioMetal",
    176 => "Super Valis IV",
    175 => "Indiana Jone’s Greatest Adventures",
    174 => "Cool Spot",
    173 => "Maui Mallard in Cold Shadow",
    172 => "Super Street Fighter II",
    171 => "Super Smash TV",
    170 => "Plok",
    169 => "Bust-A-Move",
    168 => "Super R-Type",
    167 => "Teenage Mutant Ninja Turtles: Tournament Fighters",
    166 => "Final Fight",
    165 => "Arkanoid: Doh it Again",
    164 => "Kirby’s Dream Course",
    163 => "FIFA International Soccer",
    162 => "NBA Hangtme",
    161 => "NCAA Basketball",
    160 => "Breath of Fire",
    159 => "Ms. Pacman",
    158 => "Top Gear 2",
    157 => "Firepower 2000",
    156 => "Mega Man 7",
    155 => "The Adventures of Batman and Robin",
    154 => "Super Turrican",
    153 => "Battletoads and Double Dragon: The Ultimate Team",
    152 => "X-Men: Mutant Apocalypse",
    151 => "King of Dragons",
    150 => "Ninja Gaiden Trilogy",
    149 => "Top Gear",
    148 => "Out of This World",
    147 => "Tetris and Dr.Mario",
    146 => "Mega Man X3",
    145 => "Uniracers",
    144 => "Earthworm Jim",
    143 => "Mortal Kombat II",
    142 => "Goof Troop",
    141 => "Sparkster",
    140 => "Congo’s Caper",
    139 => "Batman Returns",
    138 => "Ardy Lightfoot",
    137 => "Ken Griffey Jr.’s Winning Run",
    136 => "Samurai Shodown",
    135 => "Super Tennis",
    134 => "Demon’s Crest",
    133 => "Sunset Riders",
    132 => "Sid Meier’s Civilization",
    131 => "SkyBlazer",
    130 => "Gradius III",
    129 => "Tecmo Super Bowl",
    128 => "Castlevania: Dracula X",
    127 => "Tetris Attack",
    126 => "Harvest Moon",
    125 => "U.N. Squadron",
    124 => "Stunt Race FX",
    123 => "SimCity",
    122 => "Tiny Toon Adventures Present’s: Buster Busts Loose",
    121 => "Knights of the Round",
    120 => "Kirby’s Dream Land 3",
    119 => "Super Ghouls ‘n Ghosts",
    118 => "DinoCity",
    117 => "Kendo Rage",
    116 => "The Magical Quest Starring Mickey Mouse",
    115 => "R-Type III: The Third Lightning",
    114 => "Wild Guns",
    113 => "The Lion King",
    112 => "Axelay",
    111 => "Breath of Fire II",
    110 => "Legend of the Mystical Ninja",
    109 => "Donkey Kong Country 3: Dixie Kong’s Double Trouble!",
    108 => "Actraiser",
    107 => "Killer Instinct",
    106 => "F-Zero",
    105 => "Jelly Boy",
    104 => ["Psycho Dream", "Super Dropzone"],
    102 => "Wrecking Crew ’98",
    101 => "Super Pinball II: Amazing Odyssey",
    100 => "Asterix and Obelix",
    99 => "Xandra’s Big Adventure (Xandra no Daibōken: Valkyrie to no Deai)",
    98 => "Flying Hero",
    97 => "Cosmo Gang: The Video",
    96 => "Kirby’s Star Stacker",
    95 => "Super Variable Geo",
    94 => "Hebereke’s Popoitto",
    93 => "Go Go Ackman 3",
    92 => "Astro Go! Go!",
    91 => "Godzilla: Monster War",
    90 => "Go Go Ackman 2",
    89 => ["Go Go Ackman", "Radical Dreamers", "Sanrio World Smash Ball"],
    86 => ["Iron Commando", "Twin Bee Rainbow Bell Adventure"],
    84 => "Ghost Sweeper Mikami",
    83 => "Cotton 100%: Marchen Adventure",
    82 => "Kishin Douji Zenki: Rettou Raiden",
    81 => "Battle Mobile",
    80 => "Far East of Eden",
    79 => "Treasure Hunter G",
    78 => "Magical Drop 2",
    77 => "Famicom Detective Club Part 2",
    76 => ["Super Bomberman 4", "Super Bomberman 5"],
    74 => "Der Langrisser",
    73 => "Fire Emblem: Thracia 776",
    72 => "Gunman’s Proof",
    71 => "Mickey’s Great Adventure in Tokyo Disneyland",
    70 => "Gokujou Parodius",
    69 => "Front Mission: Gun Hazard",
    68 => "Mega Man and Bass",
    67 => "Super Fire Pro Wresting X Premium",
    66 => "Super Bonk 2",
    65 => "Sutte Hakkun",
    64 => "Aladdin",
    63 => "Cho Aniki Bakuretsu Ranta Hen",
    62 => "The Violinist of Hameln",
    61 => "Wonder Project J",
    60 => "Tales of Phantasia",
    59 => "NBA Jam: Tournament Edition",
    58 => "Worms",
    57 => "R2: Rendering Ranger",
    56 => "Marvelous: Another Treasure Island",
    55 => "Mario Super Picross",
    54 => "Live A Live",
    53 => "King of Demons",
    52 => "Pocky & Rocky",
    51 => "Pop ‘N Twinbee",
    50 => "Jikkyou Oshaberi Parodius",
    49 => "Doremi Fantasy",
    48 => "Bahamut Lagoon",
    47 => "Fire Emblem: Genalogy of the Holy War",
    46 => "Macross: Scrambled Valkyrie",
    45 => "Mega Man X2",
    44 => "Kirby Super Star",
    43 => "Umihara Kawase",
    42 => "Zombie Ate my Neighbors",
    41 => "Gundam Wing: Endless Duel",
    40 => "Front Mission",
    39 => "Star Ocean",
    38 => "Dragon Quest 5: Hand of the Heavenly Bride",
    37 => "Spanky’s Quest",
    36 => "Super Castlevania IV",
    35 => "Tactics Ogre: Let Us Cling Together",
    34 => "Secret of Mana",
    33 => "Earthbound",
    32 => "Teenage Mutant Ninja Turtles IV: Turtles in Time",
    31 => "Donkey Kong Country 2: Diddy’s Kong Quest",
    30 => "Parodius Da!",
    29 => "SoulBlazer",
    28 => "Super Metroid",
    27 => "Final Fantasy V",
    26 => "Donkey Kong Country",
    25 => "Super Back to the Future II",
    24 => "Mario and Wario",
    23 => "Street Fighter II Turbo",
    22 => "Contra III: The Alien Wars",
    21 => "Jaki Crush",
    20 => "Shin Nekketsu Kouha: Kunio-tachi no Banka (Kunio’s Jailbreak)",
    19 => "Illusion of Gaia",
    18 => "Super Mario World 2: Yoshi’s Island",
    17 => "Final Fantasy III (VI in Japan)",
    16 => "Super Mario RPG: Legend of the Seven Stars",
    15 => "Star Fox",
    14 => "Super Punch-Out!!",
    13 => "Super Puyo Puyo Tsuu Remix",
    12 => "Pretty Soldier Sailor Moon: Another Story",
    11 => "Chrono Trigger",
    10 => "Super Mario World",
    9 => "Super Mario All-Stars",
    8 => "Clock Tower",
    7 => "Mega Man X",
    6 => "Undercover Cops",
    5 => "Super Mario Kart",
    4 => "The Legend of Zelda: A Link to the Past",
    3 => "The Firemen",
    2 => "Secret of Mana 2",
    1 => "Terranigma",
];

// SQLite database file
$databaseFile = 'sqlite.SNES.db';

// Initialize SQLite database connection
try {
    $pdo = new PDO('sqlite:' . $databaseFile);
} catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}

// Create the top200 table if it doesn't exist
$createTableQuery = "CREATE TABLE IF NOT EXISTS top200 (id INTEGER PRIMARY KEY AUTOINCREMENT, filename TEXT)";
$pdo->exec($createTableQuery);

// Prepare the SQLite insertion query
$insertQuery = "INSERT INTO top200 (filename) VALUES (:filename)";
$statement = $pdo->prepare($insertQuery);

// Function to fuzzy match and insert games into the top200 table
function searchAndInsertGame($game, $statement) {
    global $pdo;

// Fuzzy match query
$fuzzyMatchQuery = "SELECT filename FROM games WHERE filename LIKE :game";
$fuzzyStatement = $pdo->prepare($fuzzyMatchQuery);

// Execute the fuzzy match query
$fuzzyStatement->execute([':game' => "%$game%"]);
$result = $fuzzyStatement->fetch(PDO::FETCH_ASSOC);

if ($result) {
    $statement->bindParam(':filename', $result['filename'], PDO::PARAM_STR);
    $statement->execute();
}

}

// Iterate through the SNES games array and search/insert into top200 table
foreach ($snesGames as $game) {
    if (is_array($game)) {
        foreach ($game as $subGame) {
            searchAndInsertGame($subGame, $statement);
        }
    } else {
        searchAndInsertGame($game, $statement);
    }
}

// Close the database connection
$pdo = null;

echo "Script executed successfully.\n";

?>
