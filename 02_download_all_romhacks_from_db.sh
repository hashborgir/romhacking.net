#!/bin/bash

# Set the path to the SQLite database file
db_file="RHDN.translations.sqlite.db"

# Define the output path
output_path="./TRANSLATION_ZIPS"

# Ensure the output directory exists
mkdir -p "$output_path"

# Function to check if a file is 0 bytes
is_zero_byte_file() {
    local file="$1"
    [ "$(wc -c < "$file")" -lt 1024 ]
}

# Function to download a single file in the background
download_file() {
    hack_id="$1"
    orig_game="$2"
    romhack_file="$3"
    console_name="$4"

    # Create the directory structure for the output file
    output_dir="$output_path/$console_name/$orig_game/$hack_id"
    mkdir -p "$output_dir"

    # Check if the existing file is 0 bytes
        # Use wget to download the file in the background
        wget --referer="https://www.romhacking.net/translations/$hack_id" \
             --load-cookies=cookies-romhacking-net.txt \
             --output-document="$output_dir/$romhack_file" \
             "https://www.romhacking.net/download/translations/$hack_id" &

        # Output progress message
        echo "Download started: $output_dir/$romhack_file"

}

# Fetch data from the database and iterate through each row
sqlite3 "$db_file" <<EOF |
.mode tabs
.headers off
SELECT hack_id, orig_game, romhack_file, console_name, category FROM hacks WHERE romhack_file IS NOT NULL;
EOF
while IFS=$'\t' read -r hack_id orig_game romhack_file console_name category; do
    # Call the function to download each file in parallel
    download_file "$hack_id" "$orig_game" "$romhack_file" "$console_name"
done

while [ $(jobs -p | wc -l) -ge 16 ]; do
    sleep 1
done



echo "All downloads complete!"
