#!/bin/bash

while IFS= read -r line; do
  hack_id=$(echo "$line" | grep -oP '/translations/\K\d+')
  url="http://www.romhacking.net/translations/$hack_id/"
  output_file="TRANSLATIONS/$hack_id.html"

  # Check if the file already exists, skip if it does
  if [ -e "$output_file" ]; then
    echo "Skipping existing file: $output_file"
    continue
  fi

  # Download the file in the background
  wget --load-cookies cookies-romhacking-net.txt "$url" -O "$output_file" &

  # Wait until there are fewer than 16 background processes
  while [ $(jobs -p | wc -l) -ge 16 ]; do
    sleep 1
  done

done < "RHDN Translations.txt"

# Wait for all background processes to finish
wait
