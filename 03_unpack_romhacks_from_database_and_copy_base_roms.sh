#!/bin/bash

# Set the path to the SQLite database file
db_file="RHDN.translations.sqlite.db"
lock_file="/tmp/your_script_lock_file.lock"

# Acquire an exclusive lock on the lock file
exec 200>"$lock_file"
flock -n 200 || { echo "Failed to acquire lock. Another instance may be running."; exit 1; }

# Enable Write-Ahead Logging (WAL) mode
sqlite3 "$db_file" <<EOF
PRAGMA journal_mode=WAL;
EOF

# Set paths
playlist_path="$HOME/Games/retroarch/playlists"
thumbnails_path="$HOME/Games/retroarch/thumbnails"
hacks_zip_path="./TRANSLATION_ZIPS"

# Function to unpack archive intelligently
unpack_archive() {
    local archive_path="$1"
    local destination="$2"

    case "$archive_path" in
        *.zip) unzip -o "$archive_path" -d "$destination" ;;
        *.7z) 7z x "$archive_path" -o"$destination" ;;
        *.rar) unrar x "$archive_path" "$destination" ;;
    esac
}

# Fetch data from the database
sqlite3 "$db_file" <<EOF |
.mode tabs
.headers off
SELECT
  hack_id,
  orig_game,
  romhack_title,
  romhack_file,
  console_name,
  category,
  base_rom,
  img_title,
  img_snap
FROM
  hacks
WHERE
  romhack_file IS NOT NULL
  AND romhack_file != ''
  AND base_rom IS NOT NULL
  AND base_rom != ''
EOF
while IFS=$'\t' read -r hack_id orig_game romhack_title romhack_file console_name category base_rom img_title img_snap; do
    # Define the destination directory for unpacking
    destination="./TRANSLATIONS/$console_name/$orig_game/$category/$romhack_title/$hack_id/"

    # Create the destination directory if it doesn't exist
    mkdir -p "$destination"

    # Create an array to store destination files
    destination_files=()

    # Unpack the archive to the destination directory
    unpack_archive "$hacks_zip_path/$console_name/$orig_game/$hack_id/$romhack_file" "$destination"

    # Copy base_rom to destination with the original filename of the .ips or .bps file
    for ips_or_bps_file in "$destination"/*.ips "$destination"/*.bps; do
        if [ -e "$ips_or_bps_file" ]; then
            destination_file="$destination/$(basename "$ips_or_bps_file" .ips).$(echo "$base_rom" | awk -F'.' '{print $NF}')"
            cp "$base_rom" "$destination_file"
            # Get the full absolute path of the destination file
            destination_file_fullpath=$(realpath "$destination_file")
            destination_files+=("$destination_file_fullpath")
        fi
    done

    # Ensure there's at least one element in the array
    if [ ${#destination_files[@]} -eq 0 ]; then
        destination_files+=("$(realpath "$destination")")
    fi

    # Convert the array to a JSON string
    destination_json=$(printf '%s\n' "${destination_files[@]}" | jq -R . | jq -s .)

    # Print debugging information
    echo "Processing: $hack_id - $destination_json"

    # Update the patches column in the database and capture errors
    sqlite3 "$db_file" <<EOF 2>> update_error.log
    UPDATE hacks
    SET patches = '$destination_json'
    WHERE hack_id = $hack_id;
EOF

    # Check for errors in the SQL execution
    if [ $? -ne 0 ]; then
        echo "Error updating database for hack_id: $hack_id. Check update_error.log for details."
    else
        echo "Database updated successfully for hack_id: $hack_id"
    fi

done

# Close and release the lock on the lock file
exec 200>&-

echo "Processing complete!"
