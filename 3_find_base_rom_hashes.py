#!/usr/bin/python

import sqlite3

# Function to update the database with hash information
def update_hashes(hack_id, crc32_values, md5_values, sha1_values):
    with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
        cursor = conn.cursor()
        cursor.execute(f'''
            UPDATE hacks
            SET base_rom_crc32 = ?, base_rom_md5 = ?, base_rom_sha1 = ?
            WHERE hack_id = ?
        ''', (crc32_values, md5_values, sha1_values, hack_id))
        conn.commit()

# Read base_rom paths from the hacks table
with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
    cursor = conn.cursor()
    cursor.execute('SELECT hack_id, base_rom FROM hacks WHERE base_rom IS NOT NULL')
    rows = cursor.fetchall()

# Process each row
for row in rows:
    hack_id, base_rom_path = row
    try:
        crc32_values = []
        md5_values = []
        sha1_values = []

        # Search in hashmap.txt
        with open('/home/stoned/Games/Roms/hashmap.txt', 'r') as hashmap_file:
            for line in hashmap_file:
                if base_rom_path in line:
                    crc32_values.append(line.split(':')[0].strip())

        # Search in hashmap.md5.txt
        with open('/home/stoned/Games/Roms/hashmap.md5.txt', 'r') as hashmap_md5_file:
            for line in hashmap_md5_file:
                if base_rom_path in line:
                    md5_values.append(line.split(':')[0].strip())

        # Search in hashmap.sha1.txt
        with open('/home/stoned/Games/Roms/hashmap.sha1.txt', 'r') as hashmap_sha1_file:
            for line in hashmap_sha1_file:
                if base_rom_path in line:
                    sha1_values.append(line.split(':')[0].strip())

        # Update the database with the extracted hash values
        update_hashes(hack_id, ",".join(crc32_values), ",".join(md5_values), ",".join(sha1_values))
        print(f"Updated hashes for hack_id {hack_id}")

    except Exception as e:
        print(f"Error processing hack_id {hack_id}: {e}")

print("Done.")
