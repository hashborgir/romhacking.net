#!/usr/bin/env python3

import argparse
import xdelta3

#define PY_SSIZE_T_CLEAN
import sys

def apply_xdelta_patch(original_file, patch_file, output_file):
    with open(original_file, 'rb') as original:
        original_data = original.read()

    with open(patch_file, 'rb') as patch:
        patch_data = patch.read()

    patched_data = xdelta3.decode(original_data, patch_data)

    with open(output_file, 'wb') as output:
        output.write(patched_data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Apply xdelta patch to a Genesis ROM file.")
    parser.add_argument("original_rom", help="Path to the original Genesis ROM file")
    parser.add_argument("xdelta_patch", help="Path to the xdelta patch file")
    parser.add_argument("output_rom", help="Path to the output patched Genesis ROM file")

    args = parser.parse_args()

    apply_xdelta_patch(args.original_rom, args.xdelta_patch, args.output_rom)
