Atari - 2600|37
Atari - 5200|42
Atari - 7800|43
Atari - Jaguar|28
Atari - Lynx|52
Coleco - ColecoVision|53
NEC - PC Engine SuperGrafx|45
NEC - PC-FX|6
Nintendo - Family Computer Disk System|7
Nintendo - Game Boy Advance|10
Nintendo - Game Boy|8
Nintendo - GameCube|33
Nintendo - Nintendo 64|27
Nintendo - Nintendo DS|23
Nintendo - Nintendo Entertainment System|1
Nintendo - Pokemon Mini|41
Nintendo - Super Nintendo Entertainment System|9
Nintendo - Virtual Boy|32
Philips - CD-i|50
Sega - 32X|31
Sega - Dreamcast|14
Sega - Game Gear|12
Sega - Master System - Mark III|22
Sega - Mega-CD - Sega CD|29
Sega - Mega Drive - Genesis|11
Sega - Saturn|13
Sega - SG-1000|34
Sharp - X68000|30
SNK - Neo Geo CD|15
SNK - Neo Geo Pocket Color|16
Sony - PlayStation|17
Sony - PlayStation 2|18
Sony - PlayStation 3|47
Sony - PlayStation Portable|44
Sony - PlayStation Vita|54
The 3DO Company - 3DO|46
