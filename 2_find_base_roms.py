#!/usr/bin/python

import json
import sqlite3

# Function to update the database with base_rom information
def update_base_rom(hack_id, base_rom_path):
    with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
        cursor = conn.cursor()
        cursor.execute('''
            UPDATE hacks
            SET base_rom = ?
            WHERE hack_id = ?
        ''', (base_rom_path, hack_id))
        conn.commit()

# Read data from the hacks table
with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
    cursor = conn.cursor()
    cursor.execute('SELECT hack_id, checksums FROM hacks WHERE checksums IS NOT NULL AND checksums != ""')
    rows = cursor.fetchall()

# Process each row
for row in rows:
    hack_id, hash_data = row
    base_rom_found = False

    try:
        # Load hash data as JSON
        hash_values = json.loads(hash_data)

        # Process CRC32 hashes
        for crc32_hash in hash_values.get("crc32", "").split(","):
            if crc32_hash:
                with open('/home/stoned/Games/Roms/hashmap.txt', 'r') as hashmap_file:
                    for line in hashmap_file:
                        if crc32_hash in line:
                            base_rom_path = line.split(':')[1].strip()
                            update_base_rom(hack_id, base_rom_path)
                            print(f"Updated base_rom for hack_id {hack_id} using CRC32.")
                            base_rom_found = True
                            break

        # Process MD5 hashes
        for md5_hash in hash_values.get("md5", "").split(","):
            if not base_rom_found and md5_hash:
                with open('/home/stoned/Games/Roms/hashmap.md5.txt', 'r') as hashmap_md5_file:
                    for line in hashmap_md5_file:
                        if md5_hash in line:
                            base_rom_path = line.split(':')[1].strip()
                            update_base_rom(hack_id, base_rom_path)
                            print(f"Updated base_rom for hack_id {hack_id} using MD5.")
                            base_rom_found = True
                            break

        # Process SHA1 hashes
        for sha1_hash in hash_values.get("sha1", "").split(","):
            if not base_rom_found and sha1_hash:
                with open('/home/stoned/Games/Roms/hashmap.sha1.txt', 'r') as hashmap_sha1_file:
                    for line in hashmap_sha1_file:
                        if sha1_hash in line:
                            base_rom_path = line.split(':')[1].strip()
                            update_base_rom(hack_id, base_rom_path)
                            print(f"Updated base_rom for hack_id {hack_id} using SHA1.")
                            base_rom_found = True
                            break

    except json.JSONDecodeError:
        print(f"Error decoding JSON for hack_id {hack_id}")

print("Done.")
