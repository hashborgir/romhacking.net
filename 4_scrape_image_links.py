#!/usr/bin/python

import os
import sqlite3
from bs4 import BeautifulSoup

# Function to update the database with image sources
def update_images(hack_id, img_title_src, img_snap_src):
    with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
        cursor = conn.cursor()
        cursor.execute(f'''
            UPDATE hacks
            SET img_title = ?,
                img_snap = ?
            WHERE hack_id = ?
        ''', (img_title_src, img_snap_src, hack_id))
        conn.commit()

# Read hack_ids from the hacks table
with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
    cursor = conn.cursor()
    cursor.execute('SELECT hack_id FROM hacks')
    hack_ids = [row[0] for row in cursor.fetchall()]

# Process each hack_id
for hack_id in hack_ids:
    html_file_path = f'TRANSLATIONS_HTML/{hack_id}.html'

    if os.path.exists(html_file_path):
        with open(html_file_path, 'r', encoding='ISO-8859-1') as html_file:
            soup = BeautifulSoup(html_file, 'html.parser')

            # Extract image sources
            images_titles = [img['src'] for img in soup.find_all('img', alt='Title Screen')]
            images_snaps = [img['src'] for img in soup.find_all('img', alt='RHDN Translation Image')]

            # Update the database with the extracted image sources
            update_images(hack_id, ",".join(images_titles), ",".join(images_snaps))
            print(f"Updated images for hack_id {hack_id}")

    else:
        print(f"HTML file not found for hack_id {hack_id}")

print("Done updating images.")
