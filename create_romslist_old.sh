#!/bin/bash

# Define the ROMs directory
roms_dir=$HOME/Games/Roms

# List of folders
folders=(
    "GBA"
    "Genesis"
    "N64"
    "NEC - PC Engine - TurboGrafx 16"
    "Nintendo - Game Boy Advance"
    "Nintendo - Game Boy Color"
    "Nintendo - Game Boy"
    "Nintendo - Nintendo Entertainment System"
    "SNES"
    "SNK - Neo Geo Pocket Color"
    "SNK - NeoGeo"
    "Sega - 32X"
    "Sega - Game Gear"
)

# Output file
output_file=$roms_dir/romslist.txt

# Ensure the output file is empty
> $output_file

# Loop through each folder
for folder in "${folders[@]}"
do
    # Find .7z and .zip files in the folder and append to romslist.txt
    find "$roms_dir/$folder" -type f \( -iname '*.7z' -o -iname '*.zip' \) -exec echo {} \; >> $output_file
done

echo "Task 1 completed. romslist.txt has been updated with the list of .7z and .zip files."
