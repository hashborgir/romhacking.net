#!/usr/bin/python

import json
import os
import re
import sqlite3
from bs4 import BeautifulSoup

# Connect to SQLite database
conn = sqlite3.connect('RHDN.translations.sqlite.db')
cursor = conn.cursor()

# Create the table if it doesn't exist
cursor.execute('''
CREATE TABLE IF NOT EXISTS "hacks" (
    "hack_id" INTEGER,
    "orig_game" TEXT,
    "romhack_title" TEXT,
    "romhack_file" TEXT,
    "released_by" TEXT,
    "released_date" TEXT,
    "genre" TEXT,
    "console_name" TEXT,
    "mods" TEXT,
    "category" TEXT,
    "version" TEXT,
    "base_rom" TEXT,
    "checksums" TEXT,
    "base_rom_crc32" TEXT,
    "base_rom_sha1" TEXT,
    "base_rom_md5" TEXT,
    "img_title" TEXT,
    "img_snap" TEXT,
    PRIMARY KEY("hack_id")
);
''')

# Read the text file and insert data into the database
with open('RHDN Translations.txt', 'r') as file:
    for line in file:
        # Split the line at '|'
        data = line.strip().split('|')

        # Insert data into the database
        cursor.execute('''
            INSERT INTO hacks (hack_id, orig_game, romhack_title, released_by, genre, console_name, mods, category, version, released_date, base_rom, checksums,
                base_rom_crc32, base_rom_sha1, base_rom_md5, img_title, img_snap)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
        ''', (int(data[0].split('/')[-2]), data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]))

# Commit the changes for the first script and close the connection
conn.commit()
conn.close()

# Function to update the database with hash information
def update_database(hack_id, hash_values):
    with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
        cursor = conn.cursor()
        cursor.execute(f'''
            UPDATE hacks
            SET checksums = ?
            WHERE hack_id = ?
        ''', (json.dumps(hash_values), hack_id))
        conn.commit()

# Function to extract hashes from text using regex patterns
def extract_hashes_from_patterns(text, patterns):
    all_hashes = []
    for pattern in patterns:
        matches = re.findall(pattern, text, re.IGNORECASE)
        if matches:
            all_hashes.extend(matches)
    return all_hashes

# Function to extract all hashes from the specified div using regex patterns
def extract_hashes_from_div(html_source, div_id, crc32_patterns, sha1_patterns, md5_patterns):
    soup = BeautifulSoup(html_source, 'html.parser')
    div_element = soup.find('div', id=div_id)
    if div_element:
        text_inside_div = div_element.get_text()
        crc32_values = extract_hashes_from_patterns(text_inside_div, crc32_patterns)
        sha1_values = extract_hashes_from_patterns(text_inside_div, sha1_patterns)
        md5_values = extract_hashes_from_patterns(text_inside_div, md5_patterns)
        return {
            "crc32": ",".join(crc32_values).lower().replace(" ", "").replace(":", ""),
            "sha1": ",".join(sha1_values).lower().replace(" ", "").replace(":", ""),
            "md5": ",".join(md5_values).lower().replace(" ", "").replace(":", ""),
        }
    return ""

# Function to process a single hack_id
def process_hack_id(hack_id):
    # Read HTML source from the local file
    file_path = f'TRANSLATIONS/{hack_id}.html'
    try:
        with open(file_path, 'r', encoding='utf-8', errors='replace') as file:
            html_source = file.read()
    except UnicodeDecodeError as e:
        print(f"Error processing hack_id {hack_id}: {e}")
        return

    # Define the regex patterns for CRC32, SHA-1, and MD5
    crc32_patterns = [
        r'CRC32[: ]*([0-9A-Fa-f]{8})',
        r'ROM CRC32[: ]*([0-9A-Fa-f]{8})',
        r'ROM CRC-32[: ]*([0-9A-Fa-f]{8})',
        r'CRC-32[: ]*([0-9A-Fa-f]{8})',
        r'File CRC32[: ]*([0-9A-Fa-f]{8})',
        r'File CRC-32[: ]*([0-9A-Fa-f]{8})',
        r'File/ROM CRC32[: ]*([0-9A-Fa-f]{8})',
        r'File/ROM CRC-32[: ]*([0-9A-Fa-f]{8})',
    ]

    sha1_patterns = [
        r'ROM SHA-1[: ]*([0-9A-Fa-f ]{40})',
        r'ROM SHA1[: ]*([0-9A-Fa-f ]{40})',
        r'SHA-1[: ]*([0-9A-Fa-f ]{40})',
        r'SHA1[: ]*([0-9A-Fa-f ]{44})',
        r'File SHA1[: ]*([0-9A-Fa-f ]{44})',
        r'File SHA-1[: ]*([0-9A-Fa-f ]{44})',
        r'File/ROM SHA-1[: ]*([0-9A-Fa-f ]{40})',
        r'File/ROM SHA1[: ]*([0-9A-Fa-f ]{40})',
    ]

    md5_patterns = [
        r'ROM MD5[: ]*([0-9A-Fa-f ]{32})',
        r'ROM MD-5[: ]*([0-9A-Fa-f ]{32})',
        r'File MD5[: ]*([0-9A-Fa-f ]{32})',
        r'File MD-5[: ]*([0-9A-Fa-f ]{32})',
        r'MD-5[: ]*([0-9A-Fa-f ]{32})',
        r'MD5[: ]*([0-9A-Fa-f ]{32})',
        r'File/ROM MD5[: ]*([0-9A-Fa-f ]{32})',
        r'File/ROM MD-5[: ]*([0-9A-Fa-f ]{32})',
    ]

    # Extract all hashes using regex patterns from the specified div
    hash_values = extract_hashes_from_div(html_source, 'rom_info', crc32_patterns, sha1_patterns, md5_patterns)

    # Update the database with the extracted hash values
    if any(hash_values.values()):
        update_database(hack_id, hash_values)

# Get a list of all hack_ids from the hacks table
with sqlite3.connect('RHDN.translations.sqlite.db', timeout=30) as conn:
    cursor = conn.cursor()
    cursor.execute('SELECT hack_id FROM hacks')
    hack_ids = [row[0] for row in cursor.fetchall()]

# Process hack_ids one at a time
for hack_id in hack_ids:
    process_hack_id(hack_id)
    print(f"Processed hack_id {hack_id}")
