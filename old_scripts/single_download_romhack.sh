#!/bin/bash

# Declare global variable
declare -g zips_directory

# Provided key-value pairs for cookies
cookies_file="cookies-romhacking-net.txt"

# Function to download files given URLs and output directory
download_romhacks() {
    url="$1"
    output_directory="$2"
    console_name="$3"

    zips_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/Zips"
    romhacks_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory"
    hashmap_file="$zips_directory/romhacks_hashmap.txt"
    playlist_path="$HOME/Games/retroarch/playlists"
    thumbnails_path="$HOME/Games/retroarch/thumbnails"

    # Ensure the output directory and hashmap file exist
    mkdir -p "$zips_directory"
    if [ -e "$hashmap_file" ]; then
        rm "$hashmap_file"
        touch "$hashmap_file"
    fi

    # Extract hack ID from the URL to use as part of the file name
    hack_id=$(echo "$url" | grep -oP '/hacks/(\d+)/$' | grep -oP '\d+')
    if [ -n "$hack_id" ]; then

        referer="https://www.romhacking.net/hacks/$hack_id"

        # Get the suggested filename from the server response
        suggested_filename=$(curl --cookie "$cookies_file" --referer "$referer" -I "$url" | grep -oP 'filename="\K[^"]+')

        # Set romhack_name by extracting text content from CSS selector #main > div:nth-child(1) > div:nth-child(1) > h2:nth-child(1)
        romhack_name=$(python3 -c "from bs4 import BeautifulSoup; import requests; soup = BeautifulSoup(requests.get('$referer').content, 'html.parser'); print(soup.select_one('#main > div:nth-child(1) > div:nth-child(1) > h2:nth-child(1)').get_text(strip=True))" | tr '&*/:`<>?\\|' '_')

        # Extract the base name without extension
        suggested_filename_noext=$(basename "$suggested_filename")
        suggested_filename_noext="${suggested_filename_noext%.*}"

        # Set romhack archive file
        romhack_zip_or_rar_file="$zips_directory"/"$suggested_filename"

        # Download the file and extract HTML source
        html_source=$(wget --quiet --load-cookies="$cookies_file" -O - "$referer")

        # Extract CRC32 values from the HTML source
        crc32_values=$(echo "$html_source" | grep -oP 'CRC32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')

        # Check other conditions and update crc32_values if needed
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'ROM CRC32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'ROM CRC-32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'CRC-32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'File/ROM CRC32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
        fi
        # If no CRC are found, we'll fall back on SHA-1
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'ROM SHA-1[: ]*\K[0-9A-Fa-f ]{40}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'SHA-1[: ]*\K[0-9A-Fa-f ]{40}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'SHA1[: ]*\K[0-9A-Fa-f ]{44}' | tr -d ' ' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'SHA-1:.*?\K[0-9A-Fa-f ]{40}' | tr -d ' ' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'File SHA-1[: ]*\K[0-9A-Fa-f ]{40}' | tr '[:upper:]' '[:lower:]')
        fi
        # If no CRC or SHA-1 is found, fall back on MD5
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'ROM MD5[: ]*\K[0-9A-Fa-f ]{32}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'File MD5[: ]*\K[0-9A-Fa-f ]{32}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'MD-5[: ]*\K[0-9A-Fa-f ]{32}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'MD5[: ]*\K[0-9A-Fa-f ]{32}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'File CRC32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
        fi
        if [ -z "$crc32_values" ]; then
            crc32_values=$(echo "$html_source" | grep -oP 'File/ROM SHA-1[: ]*\K[0-9A-Fa-f ]{40}' | tr '[:upper:]' '[:lower:]')
        fi


        # Grab ROM Name and search for that on disk, if no match found in hashmaps





        # Append hash value to hashmap
        # Check if there are multiple CRC32 values
        if [[ -n "$hack_id" && -n "$crc32_values" && -n "$suggested_filename" ]]; then
            if [[ "$crc32_values" =~ $'\n' ]]; then
                # Multiple CRC32 values found, split and save each in the hashmap
                IFS=$'\n' read -rd '' -a crc32_array <<< "$crc32_values"
                for crc32_value in "${crc32_array[@]}"; do
                    echo "hack_id: $hack_id, crc32_value: $crc32_value, url: $url"
                    echo "$(echo "$hack_id" | tr -d '\n'):$crc32_value:$suggested_filename" >> "$hashmap_file"
                done
            else
                # Single CRC32 value found, save it in the hashmap
                echo "hack_id: $hack_id, crc32_value: $crc32_values, url: $url"
                echo "$(echo "$hack_id" | tr -d '\n'):$crc32_values:$suggested_filename" >> "$hashmap_file"
            fi
        else
            # Print a message or take any specific action if any of the variables is empty
            echo "Skipping line in hashmap_file due to empty variables: hack_id=$hack_id, crc32_values=$crc32_values, suggested_filename=$suggested_filename"
        fi


        # Download the file
        wget --quiet --load-cookies="$cookies_file" --user-agent="Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0" --referer="$referer" --content-disposition -P "$zips_directory" -nc "$url"


        # Unpack each romhack into "$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/"
        # in its own directory named after $suggested_name minus extension
        # For example: "$HOME/Games/Romhacks/romhacking.net/SNES/SuperMarioWorld/SomeSMWHackv1/"
        # Where SomeSMWHackv1.zip would have been the romhack file $suggested_name

        if [[ "$romhack_zip_or_rar_file" == *.zip ]]; then
            destination_directory="$romhacks_directory/$(basename "$romhack_zip_or_rar_file" .zip)"
            unzip -o -d "$destination_directory" "$romhack_zip_or_rar_file"
        # Check if the file is a rar file
        elif [[ "$romhack_zip_or_rar_file" == *.rar ]]; then
            destination_directory="$romhacks_directory/$(basename "$romhack_zip_or_rar_file" .rar)"
            unrar x -o+ "$romhack_zip_or_rar_file" "$destination_directory/"
        # Check if the file is a 7z file
        elif [[ "$romhack_zip_or_rar_file" == *.7z ]]; then
            destination_directory="$romhacks_directory/$(basename "$romhack_zip_or_rar_file" .7z)"
            7z x -y "$romhack_zip_or_rar_file" -o"$destination_directory"/
        fi


        # Use the hack_id variable
        echo "Selecting base ROM file for https://www.romhacking.net/hacks/$hack_id"

        # Search for the hash value from romhacks_hashmap for this hack_id
        sha1_value_from_hashmap=$(grep "^$hack_id:" "$zips_directory/romhacks_hashmap.txt" | cut -d':' -f2 | head -n 1)
        suggested_name_from_hashmap=$(grep "^$hack_id:" "$zips_directory/romhacks_hashmap.txt" | cut -d':' -f3 | head -n 1)
        suggested_name_noext="${suggested_name_from_hashmap%.*}"

        echo "hash: $sha1_value_from_hashmap"
        echo "suggested filename: $suggested_name_from_hashmap"

        # If ROM's hash is found, then find the ROM file this hash belongs and set it to $selected_rom
        # First search the local hashmaps I generated
        if [ -n "$sha1_value_from_hashmap" ]; then
            # Search for the hash value in ~/Games/Roms/hashmap.txt
            rom_file_line=$(grep -i "$sha1_value_from_hashmap" "$HOME/Games/Roms/hashmap.txt")

            if [ -n "$rom_file_line" ]; then
                # Extract the path after the ':' character
                selected_rom=$(echo "$rom_file_line" | cut -d':' -f2 | head -n 1)
            fi
        fi

        # If hash not found in hashmap.txt, try hashmap.md5.txt
        if [ -z "$selected_rom" ]; then
            rom_file_line=$(grep -i "$sha1_value_from_hashmap" "$HOME/Games/Roms/hashmap.md5.txt")

            if [ -n "$rom_file_line" ]; then
                # Extract the path after the ':' character
                selected_rom=$(echo "$rom_file_line" | cut -d':' -f2 | head -n 1)
            fi
        fi

        # If hash not found in hashmap.txt, or hashmap.md5.txt, try hashmap.sha1.txt
        if [ -z "$selected_rom" ]; then
            rom_file_line=$(grep -i "$sha1_value_from_hashmap" "$HOME/Games/Roms/hashmap.sha1.txt")

            if [ -n "$rom_file_line" ]; then
                # Extract the path after the ':' character
                selected_rom=$(echo "$rom_file_line" | cut -d':' -f2 | head -n 1)
            fi
        fi

        if [ -z "$selected_rom" ]; then
            echo "$referer|$sha1_value_from_hashmap" >> "$console_name".failed.txt
        fi

        ##### Placeholder code for Internet Searching####

        echo "Copying the $selected_rom to $romhacks_directory"

        selected_rom_extension="${selected_rom##*.}"
        romhack_unpacked_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/$suggested_name_noext"

        echo "romhack_unpacked_directory $romhack_unpacked_directory/"

        # Create an array to store IPS/BPS filenames
        shopt -s nullglob  # Enable nullglob to handle empty expansions
        shopt -s globstar
        patch_files=("$romhack_unpacked_directory"/**/*.{ips,bps})
        # patch_files=("$romhack_unpacked_directory"/*.{ips,bps})
        shopt -u nullglob  # Disable nullglob to revert to the default behavior

        # For the current hackid, go to referer and grab the images
        # Save them to thumbnails/playlist name/Named_Boxarts/Orig Game -  $suggested_name_noext.png (convert to .png if needed  ) replace &*/:`<>?\| with _
        # Grab image from .entryinfo .imageGallery img src and save it

        mkdir -p "$thumbnails_path/$console_name Hacks/Named_Boxarts/"
        mkdir -p "$thumbnails_path/$console_name Hacks/Named_Snaps/"


        boxart_url=$(python -c "from bs4 import BeautifulSoup; import requests; print(BeautifulSoup(requests.get('$referer', cookies={'$cookies_file': ''}).text, 'html.parser').select_one('.entryinfo .imageGallery img')['src'])" 2>/dev/null)
        if [ -n "$boxart_url" ]; then
            boxart_filename="${output_directory} - ${romhack_name} - ${suggested_name_noext}"
            boxart_filename=$(echo "$boxart_filename" | tr '&*/:`<>?\\|' '_')
            boxart_filename="$thumbnails_path/$console_name Hacks/Named_Boxarts/$boxart_filename.png"
            wget -O "$boxart_filename" "$boxart_url"
        fi

        # Grab image from .entrybodysmall .imageGallery img src and save it
        snaps_url=$(python -c "from bs4 import BeautifulSoup; import requests; print(BeautifulSoup(requests.get('$referer', cookies={'$cookies_file': ''}).text, 'html.parser').select_one('.entrybodysmall .imageGallery img')['src'])" 2>/dev/null)
        if [ -n "$snaps_url" ]; then
            snaps_filename="${output_directory} - ${romhack_name} - ${suggested_name_noext}"
            snaps_filename=$(echo "$snaps_filename" | tr '&*/:`<>?\\|' '_')
            snaps_filename="$thumbnails_path/$console_name Hacks/Named_Snaps/$snaps_filename.png"
            wget -O "$snaps_filename" "$snaps_url"
        fi

        echo "boxart_filename $boxart_filename"
        echo "snaps_filename $snaps_filename"


        # Thumbnail filename and playlist label need to match, location can be anywhere

        # Check if the playlist file exists
        playlist_file="$playlist_path/$console_name - Hacks.lpl"
        if [ ! -e "$playlist_file" ]; then
            # Create the playlist file with initial structure using echo
            echo '{
        "version": "1.5",
        "default_core_path": "",
        "default_core_name": "",
        "label_display_mode": 0,
        "right_thumbnail_mode": 0,
        "left_thumbnail_mode": 0,
        "sort_mode": 0,
        "items": []
        }' > "$playlist_file"
        fi

        # Loop over the array of IPS/BPS filenames
        for patch_file in "${patch_files[@]}"; do
            ln -s "$patch_file" "$romhacks_directory/"

            if [ -n "$selected_rom" ] && [ -n "$hack_id" ]; then
                # Copy the selected ROM to romhacks_directory with the same name as the patch
                cp -fv "$selected_rom" "$romhacks_directory/$(basename "${patch_file%.ips}" .bps).$selected_rom_extension"

            fi

            # For each patch file...
            # Copy "thumbnails/playlist name/Named_Boxarts/Orig Game -  $suggested_name_noext.png"
            # To   "thumbnails/playlist name/Named_Boxarts/Orig Game -  $suggested_name_noext - patchfile name.png"  (so every patch file can have an image)
            # And create playlist entry

            # Copy boxart to a new filename based on patch file name
            if [ -n "$boxart_filename" ]; then
                patchfile_basename=$(basename "${patch_file%.ips}" .bps)
                patchfile_basename=$(echo "$patchfile_basename" | tr '&*/:`<>?\\|' '_')
                new_boxart_filename="$thumbnails_path/$console_name/Named_Boxarts/${output_directory} - ${romhack_name} - ${suggested_name_noext} - ${patchfile_basename}.png"
                cp -fv "$boxart_filename" "$new_boxart_filename"
            fi

            label="$output_directory - ${romhack_name} - ${suggested_name_noext}"

            # Create a new entry in the playlist JSON file
            playlist_entry='{
                "path": "'"$romhacks_directory/$(basename "${patch_file%.ips}" .bps).$selected_rom_extension"'",
                "label": "'"$label"'",
                "core_path": "DETECT",
                "core_name": "DETECT",
                "crc32": "DETECT",
                "db_name": "'"$console_name Hacks.lpl"'"
            }'

            # Check if the entry already exists in the JSON file
            entry_exists=$(jq --argjson entry "$playlist_entry" '.items | any(.path == $entry.path)' "$playlist_file")

            # If the entry doesn't exist, append it to the JSON file
            if [ "$entry_exists" != "true" ]; then
                jq --argjson entry "$playlist_entry" '.items += [$entry]' "$playlist_file" > "$playlist_file.tmp" && mv "$playlist_file.tmp" "$playlist_file"
            fi

        done
    fi
}


# Check for the correct number of command line arguments
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <url> <orig_game_name> <console_name>"
    exit 1
fi

# URL of the romhacking page
url="$1"

# Output directory for downloaded files
output_directory="$2"

# Console name
console_name="$3"

orig_game_name="$4"

# Download romhacks
download_romhacks  "$url" "$output_directory" "$console_name"
