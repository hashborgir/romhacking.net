#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <hack_list_file_path> <console_name>"
    exit 1
fi

file_path="$1"
console_name="$2"

while IFS='|' read -r hack_id orig_game title released_by genre console_name mods category version released_date; do
    if [ -n "$hack_id" ] && [ -n "$title" ]; then
        link="https://www.romhacking.net/download$hack_id"

        # Find actual_console_name_in_rdbs using grep and cut
        actual_console_name_in_rdbs=$(grep "$console_name" ConsoleNamesList-RHDN.mapped.txt | cut -d '|' -f 1)

        # ./single_download_romhack.sh "$link" "$orig_game" "$actual_console_name_in_rdbs"
        echo "./single_download_romhack.sh \"$link\" \"$orig_game\" \"$actual_console_name_in_rdbs\""
    fi
done < "$file_path"
