#!/bin/bash

# Declare global variable
declare -g zips_directory

# Provided key-value pairs for cookies
cookies_file="cookies-romhacking-net.txt"

# Function to get hack download URLs from a given URL
scrape_romhack_urls() {
    url="$1"
    # Make the request with the cookies
    response=$(wget --quiet --load-cookies="$cookies_file" -O - "$url")

    # Use Python to parse HTML and extract hack_hrefs
    hack_hrefs=$(python3 <<EOF
from bs4 import BeautifulSoup

html_content = """$response"""
soup = BeautifulSoup(html_content, 'html.parser')

# Extract hack_hrefs from within #main table selector
hack_hrefs = [a['href'] for a in soup.select('#main table a[href^="/hacks/"]')]
print('\n'.join(hack_hrefs))
EOF
    )

    for href in $hack_hrefs; do
        # Extract the actual href value
        hack_id=$(echo "$href" | grep -oP '\d+')
        download_url="https://www.romhacking.net/download/hacks/$hack_id/"
        echo "$download_url"
    done
}

# Function to download files given URLs and output directory
download_romhacks() {
    urls=("$@")
    output_directory="$1"
    console_name="$2"
    zips_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/Zips"
    romhacks_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory"
    hashmap_file="$zips_directory/romhacks_hashmap.txt"

    # Ensure the output directory and hashmap file exist
    mkdir -p "$zips_directory"
    rm "$hashmap_file"
    touch "$hashmap_file"

    for url in "${urls[@]}"; do
        # Extract hack ID from the URL to use as part of the file name
        hack_id=$(echo "$url" | grep -oP '/hacks/(\d+)/$' | grep -oP '\d+')
        if [ -n "$hack_id" ]; then

            referer="https://www.romhacking.net/hacks/$hack_id"

            # Get the suggested filename from the server response
            suggested_filename=$(curl --cookie "$cookies_file" --referer "$referer" -I "$url" | grep -oP 'filename="\K[^"]+')

            # Extract the base name without extension
            suggested_filename_noext=$(basename "$suggested_filename")
            suggested_filename_noext="${suggested_filename_noext%.*}"

            # Set romhack archive file
            romhack_zip_or_rar_file="$zips_directory"/"$suggested_filename"

            # Download the file and extract HTML source
            html_source=$(wget --quiet --load-cookies="$cookies_file" -O - "$referer")

            # Extract CRC32 values from the HTML source
            crc32_values=$(echo "$html_source" | grep -oP 'CRC32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')

            # Check other conditions and update crc32_values if needed
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'ROM CRC32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'ROM CRC-32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'File CRC32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'File CRC32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'CRC-32[: ]*\K[0-9A-Fa-f]{8}' | tr '[:upper:]' '[:lower:]')
            fi
            # If no CRC are found, we'll fall back on SHA-1
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'ROM SHA-1[: ]*\K[0-9A-Fa-f ]{40}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'SHA-1[: ]*\K[0-9A-Fa-f ]{40}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'SHA1[: ]*\K[0-9A-Fa-f ]{44}' | tr -d ' ' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'SHA-1:.*?\K[0-9A-Fa-f ]{40}' | tr -d ' ' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'File SHA-1[: ]*\K[0-9A-Fa-f ]{40}' | tr '[:upper:]' '[:lower:]')
            fi
            # If no CRC or SHA-1 is found, fall back on MD5
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'ROM MD5[: ]*\K[0-9A-Fa-f ]{32}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'File MD5[: ]*\K[0-9A-Fa-f ]{32}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'MD-5[: ]*\K[0-9A-Fa-f ]{32}' | tr '[:upper:]' '[:lower:]')
            fi
            if [ -z "$crc32_values" ]; then
                crc32_values=$(echo "$html_source" | grep -oP 'MD5[: ]*\K[0-9A-Fa-f ]{32}' | tr '[:upper:]' '[:lower:]')
            fi

            # Append hash value to hashmap
            # Check if there are multiple CRC32 values
            if [[ -n "$hack_id" && -n "$crc32_values" && -n "$suggested_filename" ]]; then
                if [[ "$crc32_values" =~ $'\n' ]]; then
                    # Multiple CRC32 values found, split and save each in the hashmap
                    IFS=$'\n' read -rd '' -a crc32_array <<< "$crc32_values"
                    for crc32_value in "${crc32_array[@]}"; do
                        echo "hack_id: $hack_id, crc32_value: $crc32_value, url: $url"
                        echo "$(echo "$hack_id" | tr -d '\n'):$crc32_value:$suggested_filename" >> "$hashmap_file"
                    done
                else
                    # Single CRC32 value found, save it in the hashmap
                    echo "hack_id: $hack_id, crc32_value: $crc32_values, url: $url"
                    echo "$(echo "$hack_id" | tr -d '\n'):$crc32_values:$suggested_filename" >> "$hashmap_file"
                fi
            else
                # Print a message or take any specific action if any of the variables is empty
                echo "Skipping line in hashmap_file due to empty variables: hack_id=$hack_id, crc32_values=$crc32_values, suggested_filename=$suggested_filename"
            fi


            # Download the file
            wget --quiet --load-cookies="$cookies_file" --user-agent="Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0" --referer="$referer" --content-disposition -P "$zips_directory" -nc "$url"


            # Unpack each romhack into "$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/"
            # in its own directory named after $suggested_name minus extension
            # For example: "$HOME/Games/Romhacks/romhacking.net/SNES/SuperMarioWorld/SomeSMWHackv1/"
            # Where SomeSMWHackv1.zip would have been the romhack file $suggested_name

            # Check if the file is a zip file
            if [[ "$romhack_zip_or_rar_file" == *.zip ]]; then
                destination_directory="$romhacks_directory/$(basename "$romhack_zip_or_rar_file" .zip)"
                unzip -o -d "$destination_directory" "$romhack_zip_or_rar_file"
            # Check if the file is a rar file
            elif [[ "$romhack_zip_or_rar_file" == *.rar ]]; then
                destination_directory="$romhacks_directory/$(basename "$romhack_zip_or_rar_file" .rar)"
                unrar e -o+ "$romhack_zip_or_rar_file" "$destination_directory/"
            fi

            # romhack has been unpacked to "$HOME/Games/Romhacks/romhacking.net/SNES/SuperMarioWorld/SomeSMWHackv1/"
            # Somehow use $suggested_name minus extension with hack_id and hash to find the correct ips/bps

            # Maybe we can store suggested name + hack_id in a separate file?
            # Or romhacks_hashmap.txt can have 8268:85a79d9c:$suggested_filename_noext -- Done.

            # Now we know which romhack was stored in which directory and which hack_id it belongs to
            # Go into "$HOME/Games/Romhacks/romhacking.net/SNES/SuperMarioWorld/SomeSMWHackv1/"
            # Copy the IPS/BPS patch to $output_directory
            # rename the rom exactly like the patch
            # Then copy the ips/bps patch from this directory to $output_directory
            # we set romhack_suggested_name=$(grep "^$hack_id:" "$zips_directory/romhacks_hashmap.txt" | cut -d':' -f3)


            # Use the hack_id variable
            echo "Selecting base ROM file for https://www.romhacking.net/hacks/$hack_id"

            # Search for the hash value from romhacks_hashmap for this hack_id
            # This is the hash listed on romhack page.
            # It is the ROM's hash, to which this hack can be applied.
            sha1_value_from_hashmap=$(grep "^$hack_id:" "$zips_directory/romhacks_hashmap.txt" | cut -d':' -f2 | head -n 1)
            suggested_name_from_hashmap=$(grep "^$hack_id:" "$zips_directory/romhacks_hashmap.txt" | cut -d':' -f3 | head -n 1)
            suggested_name_noext="${suggested_name_from_hashmap%.*}"

            echo "hash: $sha1_value_from_hashmap"
            echo "suggested filename: $suggested_name_from_hashmap"

            # If ROM's hash is found
            # Then find the ROM file this hash belongs and set it to $selected_rom
            # First search the local hashmaps I generated
            if [ -n "$sha1_value_from_hashmap" ]; then
                # Search for the hash value in ~/Games/Roms/hashmap.txt
                rom_file_line=$(grep -i "$sha1_value_from_hashmap" "$HOME/Games/Roms/hashmap.txt")

                if [ -n "$rom_file_line" ]; then
                    # Extract the path after the ':' character
                    selected_rom=$(echo "$rom_file_line" | cut -d':' -f2 | head -n 1)
                fi
            fi

            # If hash not found in hashmap.txt, try hashmap.md5.txt
            if [ -z "$selected_rom" ]; then
                rom_file_line=$(grep -i "$sha1_value_from_hashmap" "$HOME/Games/Roms/hashmap.md5.txt")

                if [ -n "$rom_file_line" ]; then
                    # Extract the path after the ':' character
                    selected_rom=$(echo "$rom_file_line" | cut -d':' -f2 | head -n 1)
                fi
            fi

            # If hash not found in hashmap.txt, or hashmap.md5.txt, try hashmap.sha1.txt
            if [ -z "$selected_rom" ]; then
                rom_file_line=$(grep -i "$sha1_value_from_hashmap" "$HOME/Games/Roms/hashmap.sha1.txt")

                if [ -n "$rom_file_line" ]; then
                    # Extract the path after the ':' character
                    selected_rom=$(echo "$rom_file_line" | cut -d':' -f2 | head -n 1)
                fi
            fi

            # If the hash is not found in our locally generated hashmaps, then we need to consult the RDBs
            # Use the node.js RDB parser, and find the hash to look for the ROM name.

            if [ -n "$sha1_value_from_hashmap" ] && [ -n "$hack_id" ] && [ ! -n "$selected_rom" ]; then
                # The variable $sha1_value_from_hashmap  is not empty
                parser_output=$(node $HOME/Games/retroarch/database/parser.mjs "$console_name" $sha1_value_from_hashmap)
                echo $parser_output
                echo $sha1_value_from_hashmap
                # exit 1

                if [ -n "$parser_output" ]; then
                    # Clear out /tmp/Roms
                    rm -rf /tmp/Roms/*

                    # Find the ROM file in $HOME/Games/Roms/$console_name/
                    found_rom=$(find "$HOME/Games/Roms/$console_name/" -type f -name "$parser_output*")

                    if [ -n "$found_rom" ]; then
                        # If the found_rom is not a .zip or .7z, copy it to /tmp/Roms
                        if [[ "$found_rom" != *.zip && "$found_rom" != *.7z ]]; then
                            cp "$found_rom" /tmp/Roms/
                        else
                            # If the found_rom is a .zip or .7z, unpack it to /tmp/Roms
                            case "$found_rom" in
                                *.zip)
                                    unzip -o "$found_rom" -d "/tmp/Roms"
                                    ;;
                                *.7z)
                                    7z x "$found_rom" -o"/tmp/Roms" -y
                                    ;;
                            esac
                        fi

                        # Get a list of all files in /tmp/Roms into an array
                        mapfile -t files_in_tmp_roms < <(find /tmp/Roms -type f)

                        # Loop through each file in the array
                        for file in "${files_in_tmp_roms[@]}"; do
                            file_hash=""

                            # Depending on sha1_value_from_hashmap, calculate the hash of the file
                            case "$sha1_value_from_hashmap" in
                                crc32)
                                    file_hash=$(crc32 "$file")
                                    ;;
                                md5sum)
                                    file_hash=$(md5sum "$file" | awk '{ print $1 }')
                                    ;;
                                sha1)
                                    file_hash=$(sha1sum "$file" | awk '{ print $1 }')
                                    ;;
                            esac

                            # Compare the file hash with sha1_value_from_hashmap
                            if [ "$file_hash" == "$sha1_value_from_hashmap" ]; then
                                selected_rom="$file"
                                break  # Exit the loop once a matching file is found
                            fi
                        done
                    else
                        echo "ROM file not found in $HOME/Games/Roms/$console_name/ ... Skipping."
                    fi
                else
                    echo "ROM file not found in RDB. Skipping."
                fi
            elif [ -n "$sha1_value_from_hashmap" ] && [ ! -n "$selected_rom" ]; then
                # If not even found in RDB
                # Prompt user for input and select the base ROM file

                rom_dir="$HOME/Games/Roms/$console_name"

                while true; do
                    read -rp "ROM hash $sha1_value_from_hashmap not found in hashmap/RDB. Enter the base ROM file name or part of it: " input
                    # Use find to search for ROM files in the specified directory
                    mapfile -d '' rom_files < <(find "$rom_dir" -type f -iname "*$input*" -print0)

                    if [ "${#rom_files[@]}" -eq 0 ]; then
                        echo "No matching ROM files found. Please try again."
                    else
                        # Present a numbered list of matching ROM files
                        echo "Matching ROM files:"
                        for ((i = 0; i < ${#rom_files[@]}; i++)); do
                            echo "$((i + 1)). ${rom_files[i]}"
                        done

                        # Prompt user to select a number
                        read -rp "Select a number to choose the base ROM file: " selection
                        if [[ "$selection" =~ ^[1-9][0-9]*$ && "$selection" -le "${#rom_files[@]}" ]]; then
                            selected_rom="${rom_files[selection - 1]}"
                            break
                        else
                            echo "Invalid selection. Please enter a valid number."
                        fi
                    fi
                done
            fi




            # Then automate trying to find this missing ROM on the Internet somewhere
            # We'll download the ROM, Unpack to /tmp/Roms, and calculate the 3 hashes for it.

            # If the calculated hashes match the sha1_value_from_hashmap from romhacks_hashmap
            #   Then set the downloaded ROM as $selected_rom

            # If eventually there is no ROM found that matches the hash from the romhack hack_id page
            #   Then we can't use that romhack, so append this hack_id:hash to failed.txt so we know which ones failed




            ##### Placeholder code for Internet Searching####




            echo "Selected base ROM file: $selected_rom"
            echo "Copying the base ROM to $romhacks_directory"

            # We have all three pieces of information for the current hack_id
            # hack_id
            # sha1_value_from_hashmap (using this we can find the rom)
            # suggested_name_from_hashmap (using this we can name the rom)

            # We also have selected_rom

            # Grab all ips/bps filenames from $output_directory/$suggested_name_from_hashmap/ and store in an array
            # Loop over array, and for each array element
            #   Symlink the ips/bps patch to $console_name/$output_directory
            #   cp $selected_rom $console_name/$output_directory

            selected_rom_extension="${selected_rom##*.}"
            romhack_unpacked_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/$suggested_name_noext"

            # Create an array to store IPS/BPS filenames
            shopt -s nullglob  # Enable nullglob to handle empty expansions
            patch_files=("$romhack_unpacked_directory"/*.ips "$romhack_unpacked_directory"/*.bps)
            shopt -u nullglob  # Disable nullglob to revert to the default behavior


            # Loop over the array of IPS/BPS filenames
            for patch_file in "${patch_files[@]}"; do
                ln -s "$patch_file" "$romhacks_directory/"

                if [ -n "$selected_rom" ] && [ -n "$hack_id" ]; then
                    # Copy the selected ROM to romhacks_directory with the same name as the patch
                    cp "$selected_rom" "$romhacks_directory/$(basename "$patch_file" .ips).$selected_rom_extension"

                fi
            done
        fi

#     if [ -n "$hack_id" ]; then
#         # Create symlinks for all ips/bps patches/xdelta patches
#         find "$romhacks_directory" -type f \( -name '*.ips' -o -name '*.bps' -o -name '*.xdelta' \) -exec ln -sf {} "$romhacks_directory/" \;
#     fi

    done
}


# Check for the correct number of command line arguments
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <url> <output_directory> <console_name>"
    exit 1
fi

# URL of the romhacking page
romhacking_url="$1"

# Output directory for downloaded files
output_directory="$2"

# Console name
console_name="$3"

# Scrape romhack URLs
romhack_urls=($(scrape_romhack_urls "$romhacking_url"))

# Download romhacks
download_romhacks "$output_directory" "$console_name" "${romhack_urls[@]}"
