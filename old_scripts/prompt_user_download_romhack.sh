#!/bin/bash

# Provided key-value pairs for cookies
cookies_file="cookies-romhacking-net.txt"

# Function to get hack download URLs from a given URL
get_hack_download_urls() {
    url="$1"
    # Make the request with the cookies
    response=$(wget --quiet --load-cookies="$cookies_file" -O - "$url")

    # Use Python to parse HTML and extract hack_hrefs
    hack_hrefs=$(python3 <<EOF
from bs4 import BeautifulSoup

html_content = """$response"""
soup = BeautifulSoup(html_content, 'html.parser')

# Extract hack_hrefs from within #main table selector
hack_hrefs = [a['href'] for a in soup.select('#main table a[href^="/hacks/"]')]
print('\n'.join(hack_hrefs))
EOF
    )

    for href in $hack_hrefs; do
        # Extract the actual href value
        hack_id=$(echo "$href" | grep -oP '\d+')
        download_url="https://www.romhacking.net/download/hacks/$hack_id/"
        echo "$download_url"
    done
}

# Function to download files given URLs and output directory
download_files() {
    urls=("$@")
    output_directory="$1"
    console_name="$2"
    zips_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/Zips"
    # Ensure the output directory exists
    mkdir -p "$zips_directory"
    # Download each file if it does not exist
    for url in "${urls[@]}"; do
        # Extract hack ID from the URL to use as part of the file name
        hack_id=$(echo "$url" | grep -oP '/hacks/\d+/$' | grep -oP '\d+')
        referer="https://www.romhacking.net/hacks/$hack_id"
        # Get the suggested filename from the server response
        suggested_filename=$(wget --load-cookies="$cookies_file" --user-agent="Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0" --referer="$referer" --content-disposition --quiet -O - "$url" | grep -oP 'filename="\K[^"]+')
        # Determine the output path
        output_path="$zips_directory/$suggested_filename"

        # Check if the file already exists, and if not, download it
        if [ ! -f "$console_name/$output_directory/$suggested_filename" ]; then
            wget --load-cookies="$cookies_file" --user-agent="Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0" --referer="$referer" --content-disposition -P "$zips_directory" -nc "$url"
        else
            echo "File $suggested_filename already exists in $console_name/$output_directory. Skipping download."
        fi
    done
}



# Function to process files in the output directory
process_files() {
    local output_directory="$1"
    console_name="$2"

    # Set paths
    zips_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/Zips"
    output_directory="$HOME/Games/Romhacks/romhacking.net/$console_name/$output_directory/"

    # Unpack everything into the output directory
    for zip_file in "$zips_directory"/*.zip; do
        destination_directory="$output_directory/$(basename "$zip_file" .zip)"
        unzip -o -d "$destination_directory" "$zip_file"
    done

    # Unpack rar files into the output directory
    for rar_file in "$zips_directory"/*.rar; do
        destination_directory="$output_directory/$(basename "$rar_file" .rar)"
        unrar e -o+ -r "$rar_file" "$destination_directory"
    done

    # Create symlinks in the current directory for all ips/bps patches in each romhack's subdirectory
    find "$output_directory" -mindepth 2 -type f \( -name '*.ips' -o -name '*.bps' \) -exec ln -s {} "$output_directory" \; 2>/dev/null
}


# Function to prompt user for input and select the base ROM file
select_base_rom() {
    local rom_dir="$1"
    local console_name="$2"
    local selected_rom

    while true; do
        read -rp "Enter the base ROM file name or part of it: " input
        # Use find to search for ROM files in the specified directory
        mapfile -d '' rom_files < <(find "$rom_dir" -type f -iname "*$input*.zip" -print0)

        if [ "${#rom_files[@]}" -eq 0 ]; then
            echo "No matching ROM files found. Please try again."
        else
            # Present a numbered list of matching ROM files
            echo "Matching ROM files:"
            for ((i = 0; i < ${#rom_files[@]}; i++)); do
                echo "$((i + 1)). ${rom_files[i]}"
            done

            # Prompt user to select a number
            read -rp "Select a number to choose the base ROM file: " selection
            if [[ "$selection" =~ ^[1-9][0-9]*$ && "$selection" -le "${#rom_files[@]}" ]]; then
                selected_rom="${rom_files[selection - 1]}"
                break
            else
                echo "Invalid selection. Please enter a valid number."
            fi
        fi
    done

    echo "Selected base ROM file: $selected_rom"
    echo "Copying the base ROM multiple times..."
    echo "OD: $console_name/$output_directory"

    # Copy the base ROM multiple times, named after each ips/bps patch
    for symlink in "$console_name/$output_directory"/*.ips "$console_name/$output_directory"/*.bps; do
        filename=$(basename "$symlink" .ips)
        filename=$(basename "$filename" .bps)
        cp "$selected_rom" "$console_name/$output_directory/$filename.zip"
    done
}



# Check for the correct number of command line arguments
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <url> <output_directory> <console_name>"
    exit 1
fi

# URL of the romhacking page
romhacking_url="$1"

# Output directory for downloaded files
output_directory="$2"

# Console name
console_name="$3"

# Get hack download URLs
download_urls=($(get_hack_download_urls "$romhacking_url"))

# Download files
download_files "$output_directory" "$console_name" "${download_urls[@]}"

# Process files in the output directory
process_files "$output_directory" "$console_name"

# Prompt user for input to select the base ROM file
rom_dir="$HOME/Games/Roms/$console_name"
select_base_rom "$rom_dir" "$console_name"

