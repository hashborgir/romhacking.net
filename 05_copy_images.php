#!/usr/bin/env php
<?php

// Connect to the SQLite database
$db_path = 'RHDN.sqlite.db';
$pdo = new PDO("sqlite:$db_path");
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Execute the SQL query for specific console names
$sql_query = '
    SELECT hack_id, orig_game, romhack_title, console_name, category, base_rom, img_title, img_snap, patches
    FROM hacks
    WHERE patches IS NOT NULL AND patches != ""
        AND console_name IN ("NES", "SNES", "GB", "GBA", "GG", "GEN", "32X")
    ORDER BY console_name, category, orig_game, romhack_title
';

$stmt = $pdo->query($sql_query);
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Define the console mapping
$consoleMapping = array(
    'NES' => 'Nintendo - Nintendo Entertainment System',
    'GBA' => 'Nintendo - Game Boy Advance',
    'GB' => 'Nintendo - Game Boy',
    'GC' => 'Nintendo - GameCube',
    'N64' => 'Nintendo - Nintendo 64',
    'NDS' => 'Nintendo - Nintendo DS',
    'PMINI' => 'Nintendo - Pokemon Mini',
    'SNES' => 'Nintendo - Super Nintendo Entertainment System',
    'VB' => 'Nintendo - Virtual Boy',
    'SGFX' => 'NEC - PC Engine SuperGrafx',
    'PSX' => 'Sony - PlayStation',
    'PS2' => 'Sony - PlayStation 2',
    'PS3' => 'Sony - PlayStation 3',
    'PSP' => 'Sony - PlayStation Portable',
    'JAG' => 'Atari - Jaguar',
    'LYNX' => 'Atari - Lynx',
    'SG1K' => 'Sega - SG-1000',
    '32X' => 'Sega - 32X',
    'DC' => 'Sega - Dreamcast',
    'GG' => 'Sega - Game Gear',
    'SMS' => 'Sega - Master System - Mark III',
    'SegaCD' => 'Sega - Mega-CD - Sega CD',
    'GEN' => 'Sega - Mega Drive - Genesis',
    'SAT' => 'Sega - Saturn',
    'NGCD' => 'SNK - Neo Geo CD',
    'NGPC' => 'SNK - Neo Geo Pocket Color',
    '3DO' => 'The 3DO Company - 3DO',
);

// Define the base path for various operations
$base_path = "/home/stoned/Games/Romhacks/romhacking.net/HACKS/";
$thumbnails_path = "/home/stoned/Games/retroarch/thumbnails/";

// Iterate through data and copy images for each patch
foreach ($data as $entry) {
    $console_name = $entry['console_name'];
    $console_full_name = $consoleMapping[$console_name];
    $category = $entry['category'];

    $snap_path = "$thumbnails_path$console_full_name - RHDN - $category/Named_Snaps/";
    $title_path = "$thumbnails_path$console_full_name - RHDN - $category/Named_Titles/";

    // Ensure the directories exist
    if (!is_dir($snap_path)) {
        if (!mkdir($snap_path, 0777, true)) {
            echo "Error creating directory: $snap_path\n";
            continue;
        }
    }

    if (!is_dir($title_path)) {
        if (!mkdir($title_path, 0777, true)) {
            echo "Error creating directory: $title_path\n";
            continue;
        }
    }

    // Decode the patches field
    $patches = json_decode($entry['patches']);

    // Iterate through patches and copy images
    foreach ($patches as $patch) {
        $patch_path = trim($patch);
        $patch_name = pathinfo($patch_path, PATHINFO_FILENAME);
        $label_for_patch = "{$entry['orig_game']} - {$entry['hack_id']} - {$entry['romhack_title']} - " . $patch_name;

        $img_snap_src = $base_path . 'IMAGES/snaps/' . $entry['img_snap'];
        $img_title_src = $base_path . 'IMAGES/titles/' . $entry['img_title'];

        var_dump($patch);

        // Copy snap image
        if (file_exists($img_snap_src)) {
            $snap_dest_file = "$snap_path$label_for_patch." . pathinfo($entry['img_snap'], PATHINFO_EXTENSION);

            if (!copy($img_snap_src, $snap_dest_file)) {
                echo "Error copying image: $img_snap_src to $snap_dest_file\n";
            }
        }

        // Copy title image
        if (file_exists($img_title_src)) {
            $title_dest_file = "$title_path$label_for_patch." . pathinfo($entry['img_title'], PATHINFO_EXTENSION);
            if (!copy($img_title_src, $title_dest_file)) {
                echo "Error copying image: $img_title_src to $title_dest_file\n";
            }
        }
    }
}

// Close the database connection
$pdo = null;

echo "Images copied successfully.\n";
?>
